
The base installation profile is a profile that I'm trying to integrate into my Drupal workflow when setting up a new site. Using it as is, you'll have a basic site setup with CCK / Views, an image content type. I'll be looking to extend this functionality, so please feel free to make suggestions in the issue queue.

CCK DUMPS ---
You can import your own CCK content types during this profile's install, by simply copying and pasting the export into a php file (with <?php at the start) and then pasting the dump. Name the file:
[content-type].cck.inc

and place inside the CCK directory


VIEWS DUMPS ---
Along the same lines, you can also import views dumps, except this isn't as easy of a copy and paste as CCK. On the Views export screen, you're given two boxes full of information, we're only interested in the bottom box. Copy the body of the function (between the { and }), and paste it into a php file (again with <?php at the start). Also remove the "return $views;" line found at the end of the text;

Again, place this inside the views directory of the install profile, and name the dump/s: [something].views.inc


